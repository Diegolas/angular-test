import { Component } from '@angular/core';
import { FormService } from "../../services/form.service";

@Component({
  selector: 'table-form-app',
  templateUrl: './table-form.component.html',
  styleUrls: ['./table-form.component.sass']
})
export class TableFormComponent {
  state:string = 'waiting';
  dataArry:Array<any>;

  data:any = {
    json: null,
    sort: null,
    values: null
  }

  constructor(private formService: FormService ) {

  }
  
  getForm() {
    this.formService.getForm().subscribe( form => {
        this.state = !form.success || !form.data ? 'fail' : 'success';
        if(this.state === 'success') {
          this.data = this.parseData(form.data);
        }
    } )
  }

  parseData(data:Array<any>) {
    let json = {};
    let values = [];
    data.forEach((value, index) => {
      if(!json[value]) {
        json[value] = {
          count: 0,
          minIndex: Infinity,
          maxIndex: -1
        }
        values = [value, ...values];
      }
      json[value].count++; 
      json[value].minIndex = Math.min(json[value].minIndex, index);
      json[value].maxIndex = Math.max(json[value].maxIndex, index);
    });
    const sort = Object.keys(json);
    return {json, values, sort};
  }
}
