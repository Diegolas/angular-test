import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from "@angular/router";

import { TableFormComponent } from './table-form/table-form.component';
import { TableParagraphComponent } from './table-paragraph/table-paragraph.component';
import { SideBarComponent } from './sidebar/sidebar.component';


const importExports = [
    TableFormComponent,
    SideBarComponent,
    TableParagraphComponent
]

@NgModule({
  declarations: [
    ...importExports
  ],
  imports: [
    BrowserModule,
    RouterModule
  ],
  exports: [
    ...importExports
  ],
  providers: []
})
export class ViewsModule { }
