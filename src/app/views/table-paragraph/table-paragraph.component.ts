import { Component } from '@angular/core';
import { FormService } from "../../services/form.service";

@Component({
  selector: 'table-paragraph-app',
  templateUrl: './table-paragraph.component.html',
  styleUrls: ['./table-paragraph.component.sass']
})
export class TableParagraphComponent {
  alphabet:any;
  alphabetValues:Array<string>;
  sum:Array<any>
  constructor(private formService: FormService ) {

  }
  
  getDict() {
    this.formService.getDict().subscribe( form => {
      const alphabet = this.createAlphabet();
      const data:Array<any> = form.data;
      let sum = [];
      data.forEach(value => {
        const paragraph = (<string> value.paragraph);
        const arr = paragraph.split('');
        arr.forEach(letter => {
          const lowLetter = letter.toLowerCase();
          alphabet[lowLetter] && alphabet[lowLetter].count++;
        })
        let numbers = (paragraph.match(/\d+/g) || []).map(Number);
        sum.push( { sum: numbers.length ? numbers.reduce( (a,b) => a + b ) : 0, numbers}  );
      })
      this.alphabet = alphabet;
      this.alphabetValues = Object.keys(alphabet);
      this.sum = sum;
    })
  }

  

  createAlphabet() {
    let alphabet = {};
    let indexA = 'a'.charCodeAt(0);
    const indexZ = 'z'.charCodeAt(0);
    for(; indexA <= indexZ; indexA++) {
      const letter = String.fromCharCode(indexA);
      alphabet[letter] = {count: 0, sum:0};
    }
    return alphabet;
  }
}
