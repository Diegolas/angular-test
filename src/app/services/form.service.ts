import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IRemoteResponse } from "../interfaces/IRemoteResponse";
 
@Injectable()
export class FormService {
    FORM_PATH = 'http://168.232.165.184/prueba/array';
    DICT_PATH = 'http://168.232.165.184/prueba/dict';

    constructor(private http:HttpClient) {}
 
    getForm():Observable<IRemoteResponse> {
        return this.http.get<IRemoteResponse>(this.FORM_PATH);
    }
    getDict():Observable<IRemoteResponse> {
        return this.http.get<IRemoteResponse>(this.DICT_PATH);
    }
}