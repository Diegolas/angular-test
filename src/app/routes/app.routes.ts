import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from "@angular/core";

import { TableFormComponent } from "../views/table-form/table-form.component";
import { TableParagraphComponent } from "../views/table-paragraph/table-paragraph.component";

const appRoutes: Routes = [
    { path: '', component: TableFormComponent },
    { path: 'tableform', component: TableFormComponent },
    { path: 'paragraph', component: TableParagraphComponent }
];

export const appRoutingProviders:any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);